﻿using System;
using System.Drawing;
using System.Text.RegularExpressions;

namespace Phase10.CLI.Common.Console
{
    // {f:#00}
    // {f:#000}
    // {f:#000000}
    // {f:red}
    // {f:darkRed}

    // {[f|b]:(#[0-9a-fA-F]{2,6}|[a-zA-Z]+)}

    public class JConsole
    {
        private static readonly Regex RegexDelimiter = new Regex(@"(\{(?:[^\}]+)\})", RegexOptions.Compiled);

        private static readonly Regex RegexInner = new Regex(@"\{([f|b])(?::(#[0-9a-fA-F]{2,6}|[a-zA-Z]+))?\}",
            RegexOptions.Compiled);

        public static void WriteLine(string format, params object[] args)
        {
            Write(format, args);
            Write(Environment.NewLine);
        }

        public static void Write(string format, params object[] args)
        {
            var fgColor = System.Console.ForegroundColor;
            var bgColor = System.Console.BackgroundColor;

            format = RegexInner.Replace(format, @"{$0}");
            format = string.Format(format, args);

            var parts = RegexDelimiter.Split(format);

            foreach (var part in parts)
            {
                var match = RegexInner.Match(part);

                if (match.Success)
                {
                    var action = match.Groups[1].Value;
                    var color = match.Groups[2].Value;
                    var reset = string.IsNullOrEmpty(color);
                    var conColor = TranslateConsoleColor(color);

                    switch (action)
                    {
                        case "f":
                            {
                                System.Console.ForegroundColor = reset ? fgColor : conColor;
                                break;
                            }

                        case "b":
                            {
                                System.Console.BackgroundColor = reset ? bgColor : conColor;
                                break;
                            }
                    }
                }
                else
                {
                    System.Console.Write(part);
                }
            }

            System.Console.ForegroundColor = fgColor;
            System.Console.BackgroundColor = bgColor;
        }

        protected static ConsoleColor TranslateConsoleColor(string color)
        {
            var col = TranslateColor(color);
            return ClosestConsoleColor(col.R, col.G, col.B);
        }

        protected static Color TranslateColor(string color)
        {
            var col = Color.Empty;

            try
            {
                col = ColorTranslator.FromHtml(color);
            }
            catch (Exception)
            {
            }

            return col;
        }

        protected static ConsoleColor ClosestConsoleColor(byte r, byte g, byte b)
        {
            ConsoleColor ret = 0;
            double rr = r, gg = g, bb = b, delta = double.MaxValue;

            foreach (ConsoleColor cc in Enum.GetValues(typeof(ConsoleColor)))
            {
                var n = Enum.GetName(typeof(ConsoleColor), cc);
                var c = Color.FromName(n == "DarkYellow" ? "Orange" : n); // bug fix
                var t = Math.Pow(c.R - rr, 2.0) + Math.Pow(c.G - gg, 2.0) + Math.Pow(c.B - bb, 2.0);
                if (Math.Abs(t) < 1e-2)
                    return cc;
                if (t < delta)
                {
                    delta = t;
                    ret = cc;
                }
            }
            return ret;
        }
    }
}
