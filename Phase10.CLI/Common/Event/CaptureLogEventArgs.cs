﻿using System;

namespace Phase10.CLI.Common.Event
{
    public class CaptureLogEventArgs : EventArgs
    {
        public string Level { get; set; }
        public string Message { get; set; }
        public object Logger { get; set; }
        public object Custom { get; set; }
    }
}
