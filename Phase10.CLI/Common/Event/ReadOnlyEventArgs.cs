﻿using System;

namespace Phase10.CLI.Common.Event
{
    public class ReadOnlyEventArgs<T> : EventArgs
    {
        public ReadOnlyEventArgs(T input)
        {
            Parameter = input;
        }

        public T Parameter { get; private set; }
    }

    public class EventArgs<T> : EventArgs
    {
        public EventArgs(T input)
        {
            Parameter = input;
        }

        public T Parameter { get; set; }
    }
}
