﻿using System;
using System.ComponentModel;
using Phase10.CLI.Common.Event;

namespace Phase10.CLI.Common.Extensions
{
    public static class EventHandlerExtensions
    {
        public static void Raise(this EventHandler handler, object sender)
        {
            if (handler != null)
            {
                handler(sender, EventArgs.Empty);
            }
        }

        public static void Raise(this PropertyChangedEventHandler handler, object sender, string propertyName)
        {
            if (handler != null)
            {
                handler(sender, new PropertyChangedEventArgs(propertyName));
            }
        }

        public static void Raise<T>(this Delegate handler, object sender, T args) where T : EventArgs
        {
            if (handler != null)
            {
                handler.DynamicInvoke(sender, args);
            }
        }

        public static void Raise<T>(this Delegate handler, object sender, Func<T> args) where T : EventArgs
        {
            if (handler != null)
            {
                handler.DynamicInvoke(sender, args());
            }
        }

        public static EventArgs<T> CreateArgs<T>(this EventHandler<EventArgs<T>> handler, T input)
        {
            return new EventArgs<T>(input);
        }

        public static ReadOnlyEventArgs<T> CreateReadOnlyArgs<T>(this EventHandler<ReadOnlyEventArgs<T>> handler,
            T input)
        {
            return new ReadOnlyEventArgs<T>(input);
        }

        public static ReadOnlyEventArgs<Tuple<T1, T2>> CreateReadOnlyArgs<T1, T2>(
            this EventHandler<ReadOnlyEventArgs<T1>> handler, T1 input1, T2 input2)
        {
            return new ReadOnlyEventArgs<Tuple<T1, T2>>(Tuple.Create(input1, input2));
        }

        public static ReadOnlyEventArgs<Tuple<T1, T2, T3>> CreateReadOnlyArgs<T1, T2, T3>(
            this EventHandler<ReadOnlyEventArgs<T1>> handler, T1 input1, T2 input2, T3 input3)
        {
            return new ReadOnlyEventArgs<Tuple<T1, T2, T3>>(Tuple.Create(input1, input2, input3));
        }
    }
}
