﻿using System;
using NLog;
using NLog.Config;
using NLog.Targets;
using Phase10.CLI.Common.Event;
using Phase10.CLI.Common.Extensions;

namespace Phase10.CLI.Common.Log
{
    public static class Capture
    {
        private static readonly LoggingRule _rule;

        static Capture()
        {
            var config = LogManager.Configuration ?? new LoggingConfiguration();

            var methodCallTarget = new MethodCallTarget
            {
                MethodName = "OnLog",
                ClassName = typeof(Capture).AssemblyQualifiedName
            };

            methodCallTarget.Parameters.Add(new MethodCallParameter("${level}"));
            methodCallTarget.Parameters.Add(new MethodCallParameter("${message}"));
            methodCallTarget.Parameters.Add(new MethodCallParameter("${logger}"));
            methodCallTarget.Parameters.Add(new MethodCallParameter("${event-context:custom}"));

            _rule = new LoggingRule("*", LogLevel.Trace, methodCallTarget);

            config.AddTarget("method", methodCallTarget);
            config.LoggingRules.Add(_rule);

            LogManager.Configuration = config;
        }

        public static LoggingRule Rule
        {
            get { return _rule; }
        }

        public static event EventHandler<ReadOnlyEventArgs<CaptureLogEventArgs>> Message;

        public static void OnLog(string level, string message, string logger, object custom)
        {
            Message.Raise(null, Message.CreateReadOnlyArgs(new CaptureLogEventArgs
            {
                Level = level,
                Message = message,
                Custom = custom,
                Logger = logger
            }));
        }
    }
}
