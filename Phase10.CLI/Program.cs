﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using NLog;
using Phase10.CLI.Common.Console;
using Phase10.CLI.Common.Log;
using Phase10.Core;
using Phase10.Core.Players;
using Phase10.Core.Rules.Moves;

namespace Phase10.CLI
{
    public class Program
    {
        public static void Main()
        {
            ConfigureConsole();
            ConfigureLogging();

            var game = new Game();
            game.AddPlayers(
                new Player
                {
                    Name = "Alice"
                },
                new Player
                {
                    Name = "Bob"
                },
                new Player
                {
                    Name = "Eve"
                },
                new Player
                {
                    Name = "Mallory"
                }
            );

            game.Start();
            game.Reset();
            game.DealCards();

            PrintPlayerHands(game);
            Console.WriteLine("Draw pile: {0}/108 cards", game.DrawPile.Count);
            Console.WriteLine("Deposit card: {0}", game.DiscardPile.Peek());

            game.PerformMove(new DrawCardMove());
            game.PerformMove(new AutoPutPhaseMove());

            PrintPlayerHands(game);
            Console.WriteLine("Draw pile: {0}/108 cards", game.DrawPile.Count);
            Console.WriteLine("Deposit card: {0}", game.DiscardPile.Peek());
            Console.WriteLine("Current phase: {0}",
                game.CurrentPlayer.CurrentPhase.Combinations.Select(c => c.Cards)
                    .Select(c => c.ToString())
                    .Aggregate("", ((s, s1) => s + Environment.NewLine + s1)));

            Console.ReadLine();
        }

        private static void PrintPlayerHands(Game game)
        {
            Console.WriteLine();

            foreach (var player in game.Players)
            {
                Console.WriteLine("{0,-10}: {1}", player.Name, player.Hand);
            }

            Console.WriteLine();
        }

        #region - Console Window Positioning -

        const int SWP_NOSIZE = 0x0001;

        [DllImport("kernel32.dll")]
        private static extern IntPtr GetConsoleWindow();

        private static readonly IntPtr HWndConsole = GetConsoleWindow();

        [DllImport("user32.dll")]
        public static extern IntPtr SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int x, int Y, int cx, int cy, int wFlags);

        [StructLayout(LayoutKind.Sequential)]
        public struct Win32Rect
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;
        }

        [DllImport("user32.dll")]
        private static extern bool GetWindowRect(IntPtr hWnd, ref Win32Rect rect);

        private static void CenterConsoleWindow()
        {
            var rect = new Win32Rect();
            GetWindowRect(HWndConsole, ref rect);

            var screenSize = Screen.FromHandle(HWndConsole).Bounds;
            var xpos = screenSize.Width / 2 - (rect.Right - rect.Left + 1) / 2;
            var ypos = screenSize.Height / 2 - (rect.Bottom - rect.Top + 1) / 2;

            SetWindowPos(HWndConsole, 0, xpos, ypos, 0, 0, SWP_NOSIZE);
        }

        #endregion

        private readonly static Logger Logger = LogManager.GetLogger("Phase10.CLI");

        private static void ConfigureConsole()
        {
            Console.SetWindowSize(170, 45);
            Console.SetBufferSize(170, 500);
            Console.SetWindowPosition(0, 0);

            CenterConsoleWindow();
        }

        private static void ConfigureLogging()
        {
            Capture.Message += (sender, args) =>
            {
                var p = args.Parameter;

                JConsole.Write("[{0}] ", p.Level.ToString().Substring(0, 1));

                switch (p.Level.ToLower())
                {
                    case "trace":
                    case "debug":
                        JConsole.WriteLine("{f:green}{0}", p.Message);
                        break;
                    case "warn":
                        JConsole.WriteLine("{f:yellow}{0}", p.Message);
                        break;
                    case "info":
                        JConsole.WriteLine("{f:cyan}{0}", p.Message);
                        break;
                    case "fatal":
                    case "error":
                        JConsole.WriteLine("{f:red}{0}", p.Message);
                        break;
                }
            };
            //Capture.Rule.DisableLoggingForLevel(LogLevel.Debug);
            //Capture.Rule.DisableLoggingForLevel(LogLevel.Trace);
        }

        private static void ProfileAction(string name, Action action, LogLevel logLevel = null)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            action();

            stopwatch.Stop();

            Logger.Log(logLevel ?? LogLevel.Info, String.Format("{0} takes {1}ms", name, stopwatch.ElapsedMilliseconds));
        }
    }
}
