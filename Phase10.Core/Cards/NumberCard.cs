﻿namespace Phase10.Core.Cards
{
    public sealed class NumberCard : Card
    {
        public CardColor Color { get; set; }
        public int Value { get; set; }

        public NumberCard(CardColor color, int value)
        {
            Color = color;
            Value = value;
        }

        public override string ToString()
        {
            return string.Format("{0}{1,-2}", Color.ToString().Substring(0, 1), Value);
        }

        public override bool Equals(object obj)
        {
            var numberCard = obj as NumberCard;
            return numberCard != null && (numberCard.Value == Value && numberCard.Color == Color);
        }

        public override int GetHashCode()
        {
            return 17 * Color.GetHashCode() + 3 * Value;
        }
    }

    public enum CardColor
    {
        Red,
        Yellow,
        Green,
        Purple
    }
}
