﻿namespace Phase10.Core.Cards
{
    public sealed class SuspendCard : Card
    {
        public override string ToString()
        {
            return "S  ";
        }

        public override bool Equals(object obj)
        {
            var numberCard = obj as SuspendCard;
            return numberCard != null;
        }

        public override int GetHashCode()
        {
            return 17 * ToString().GetHashCode();
        }
    }
}
