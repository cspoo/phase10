﻿namespace Phase10.Core.Cards
{
    public sealed class Wildcard : Card
    {
        public override string ToString()
        {
            return "W  ";
        }

        public override bool Equals(object obj)
        {
            var numberCard = obj as Wildcard;
            return numberCard != null;
        }

        public override int GetHashCode()
        {
            return 17 * ToString().GetHashCode();
        }
    }
}
