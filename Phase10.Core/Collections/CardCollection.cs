﻿using System;
using System.Collections.Generic;
using System.Linq;
using Phase10.Core.Cards;
using PostSharp.Patterns.Threading;

namespace Phase10.Core.Collections
{
    [ReaderWriterSynchronized]
    public class CardCollection : LinkedList<Card>
    {
        public static IEnumerable<Card> AllCards
        {
            get
            {
                foreach (CardColor color in Enum.GetValues(typeof (CardColor)))
                    foreach (var i in Enumerable.Range(1, 12))
                    {
                        yield return new NumberCard(color, i);
                        yield return new NumberCard(color, i);
                    }

                foreach (var i in Enumerable.Range(1, 8))
                    yield return new Wildcard();

                foreach (var i in Enumerable.Range(1, 4))
                    yield return new SuspendCard();
            }
        }

        public CardCollection()
        {
        }

        public CardCollection(IEnumerable<Card> cards) : this()
        {
            foreach (var card in cards)
            {
                Add(card);
            }
        }

        public CardCollection(params Card[] cards) : this(cards.AsEnumerable())
        {
        }

        [Writer]
        public void Add(Card item)
        {
            ((ICollection<Card>) this).Add(item);
        }

        [Writer]
        public Card DrawCard()
        {
            if (!this.Any())
                return null;

            try
            {
                var card = First.Value;
                RemoveFirst();
                return card;
            }
            catch (Exception)
            {
                return null;
            }
        }
        
        [Reader]
        public override string ToString()
        {
            return string.Join(" ", this);
        }
    }
}
