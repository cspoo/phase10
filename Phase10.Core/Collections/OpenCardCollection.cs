﻿using Phase10.Core.Cards;
using PostSharp.Patterns.Threading;

namespace Phase10.Core.Collections
{
    [ReaderWriterSynchronized]
    public class OpenCardCollection : CardCollection
    {
        [Reader]
        public Card Peek()
        {
            return First != null ? First.Value : null;
        }
    }
}
