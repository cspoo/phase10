﻿using System;
using System.Collections.Generic;
using PostSharp.Patterns.Contracts;

namespace Phase10.Core.Common.Extensions
{
    public static class EnumerableExtensions
    {
        public static void ForEach<T>([NotNull] this IEnumerable<T> e, Action<T> action)
        {
            foreach (var item in e)
                action(item);
        }
    }
}
