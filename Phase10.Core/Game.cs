﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using NLog;
using Phase10.Core.Cards;
using Phase10.Core.Collections;
using Phase10.Core.Common.Extensions;
using Phase10.Core.Players;
using Phase10.Core.Rules.Moves;
using Phase10.Core.Rules.Phases;
using PostSharp.Patterns.Contracts;
using PostSharp.Patterns.Model;
using PostSharp.Patterns.Threading;
using Stateless;

namespace Phase10.Core
{
    public enum GameState
    {
        Begin,
        CardDrawn,
        PhasePut,
        CardsAppended,
        CardDiscarded
    }

    public enum GameMove
    {
        DrawCard,
        PutPhase,
        AppendCards,
        DiscardCard,
        Reset
    }

    [Export]
    [PartCreationPolicy(CreationPolicy.Shared)]
    [ReaderWriterSynchronized]
    public class Game
    {
        private readonly static Logger Logger = LogManager.GetLogger("Phase10.Core");

        [Reference]
        private readonly LinkedList<Player> _players = new LinkedList<Player>();
        
        public int CurrentPlayerIndex { get; private set; }

        [Reference]
        private readonly StateMachine<GameState, GameMove> _gameState =
            new StateMachine<GameState, GameMove>(GameState.Begin);
        
        [Child]
        public CardCollection DrawPile { get; private set; }
        [Child]
        public OpenCardCollection DiscardPile { get; private set; }

        public Player CurrentPlayer
        {
            get
            {
                return _players.ElementAtOrDefault(CurrentPlayerIndex);
            }
        }

        public IEnumerable<Player> Players
        {
            get { return _players.AsEnumerable(); }
        }

        public Game()
        {
            DrawPile = new CardCollection();
            DiscardPile = new OpenCardCollection();

            _gameState.Configure(GameState.Begin)
                .PermitReentry(GameMove.Reset)
                .Permit(GameMove.DrawCard, GameState.CardDrawn);

            _gameState.Configure(GameState.CardDrawn)
                .Permit(GameMove.Reset, GameState.Begin)
                .Permit(GameMove.PutPhase, GameState.PhasePut)
                .PermitIf(GameMove.AppendCards, GameState.CardsAppended, () => CurrentPlayer.PhaseCompleted);

            _gameState.Configure(GameState.PhasePut)
                .Permit(GameMove.Reset, GameState.Begin)
                .Permit(GameMove.AppendCards, GameState.CardsAppended)
                .Permit(GameMove.DiscardCard, GameState.CardDiscarded);

            _gameState.Configure(GameState.CardsAppended)
                .Permit(GameMove.Reset, GameState.Begin)
                .Permit(GameMove.DiscardCard, GameState.CardDiscarded);
        }

        private static GameMove GameMoveFromMove([NotNull] IMove move)
        {
            if (move is AppendCardsMove)
                return GameMove.AppendCards;
            if (move is DiscardCardMove)
                return GameMove.DiscardCard;
            if (move is DrawCardMove)
                return GameMove.DrawCard;
            if (move is PutPhaseMove)
                return GameMove.PutPhase;

            throw new ArgumentOutOfRangeException("move");
        }

        [Reader]
        public bool MoveIsValid([NotNull] IMove move)
        {
            return _gameState.CanFire(GameMoveFromMove(move));
        }

        [Writer]
        public void PerformMove([NotNull] IMove move)
        {
            if (!MoveIsValid(move))
                return;

            if (move.Perform(this, CurrentPlayer))
            {
                _gameState.Fire(GameMoveFromMove(move));
            }
        }

        [Writer]
        public void Start()
        {
            _players.ForEach(p => p.CurrentPhase = new Phase1());
        }

        [Writer]
        public void Reset()
        {
            DiscardPile.Clear();
            DrawPile.Clear();
            CardCollection.AllCards.OrderBy(elem => Guid.NewGuid()).ForEach(DrawPile.Add);

            // Put a single numeric card on the discard pile (i.e. draw cards till a numeric card
            // appears)
            do
            {
                var card = DrawPile.DrawCard();
                DiscardPile.AddFirst(card);
            } while (!(DiscardPile.Peek() is NumberCard));
        }

        [Writer]
        public void DealCards()
        {
            for (var i = 0; i < 10; ++i)
            {
                foreach (var player in _players)
                {
                    player.Hand.Add(DrawPile.First.Value);
                    DrawPile.RemoveFirst();
                }
            }

            CurrentPlayerIndex = 0;
        }

        [Writer]
        public void AddPlayer([NotNull] Player player)
        {
            _players.AddLast(player);
        }

        [Writer]
        public void AddPlayers([NotNull] IEnumerable<Player> players)
        {
            foreach (var player in players)
            {
                _players.AddLast(player);
            }
        }

        [Writer]
        public void AddPlayers(params Player[] players)
        {
            AddPlayers(players.AsEnumerable());
        }

        [Writer]
        public void CloseRound()
        {
            AdvanceToNextPlayer();
        }

        [Writer]
        private void AdvanceToNextPlayer()
        {
            CurrentPlayerIndex = CurrentPlayerIndex == _players.Count - 1 ? CurrentPlayerIndex + 1 : 0;
            if (DiscardPile.Peek() is SuspendCard)
            {
                CurrentPlayerIndex = CurrentPlayerIndex == _players.Count - 1 ? CurrentPlayerIndex + 1 : 0;
            }

            _gameState.Fire(GameMove.Reset);
        }
    }
}
