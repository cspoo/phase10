﻿using Phase10.Core.Collections;
using Phase10.Core.Rules.Phases;

namespace Phase10.Core.Players
{
    public class Player
    {
        public string Name { get; set; }
        public Phase CurrentPhase { get; set; }
        public bool PhaseCompleted { get; set; }
        public CardCollection Hand { get; set; }
        public int Score { get; set; }

        public Player()
        {
            Hand = new CardCollection();
        }
    }
}
