﻿using System.Collections.Generic;
using Phase10.Core.Cards;
using PostSharp.Patterns.Contracts;

namespace Phase10.Core.Rules.Combinations
{
    public interface ICombination
    {
        bool CanAppendCard([NotNull] Card card);
        void AppendCard([NotNull] Card card);
        IEnumerable<Card> Cards { get; }
    }
}
