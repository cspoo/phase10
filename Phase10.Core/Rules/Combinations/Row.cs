﻿using System.Collections.Generic;
using System.Linq;
using Phase10.Core.Cards;
using Phase10.Core.Collections;
using PostSharp.Patterns.Contracts;

namespace Phase10.Core.Rules.Combinations
{
    public class Row : ICombination
    {
        public Row([NotNull] IEnumerable<Card> cards)
        {
            var cardList = cards.ToList();

            if (IsValidRow(cardList))
            {
                cardList.ForEach(_cards.Add);
            }
        }

        private static bool IsValidRow([NotNull] IList<Card> cards)
        {
            if (!cards.All(c => (c is NumberCard || c is Wildcard)))
                return false;

            var offset = 0;
            var firstNumber = 0;
            foreach (var card in cards)
            {
                if (card is Wildcard)
                {
                    offset++;
                    continue;
                }

                var numberCard = card as NumberCard;
                if (numberCard == null)
                    return false;

                firstNumber = numberCard.Value;
                break;
            }

            if (firstNumber == 0)
                return false;

            var n = firstNumber - offset;
            foreach (var card in cards)
            {
                if (card is Wildcard)
                {
                    n++;
                    continue;
                }

                var numberCard = card as NumberCard;
                if (numberCard == null)
                    return false;

                if (numberCard.Value != n)
                    return false;

                n++;
            }

            return true;
        }

        public bool CanAppendCard(Card card)
        {
            return CanAppendFirst(card) || CanAppendLast(card);
        }

        private bool CanAppendFirst(Card card)
        {
            if (card is Wildcard)
                return true;

            if (!(card is NumberCard))
                return false;

            var n = 0;
            foreach (var numberCard in _cards.Select(c => c as NumberCard))
            {
                if (numberCard != null)
                    return numberCard.Value - n - 1 == ((NumberCard) card).Value;

                n++;
            }

            return false;
        }

        private bool CanAppendLast(Card card)
        {
            if (card is Wildcard)
                return true;

            if (!(card is NumberCard))
                return false;

            var n = 0;
            foreach (var numberCard in _cards.Reverse().Select(c => c as NumberCard))
            {
                if (numberCard != null)
                    return numberCard.Value + n + 1 == ((NumberCard)card).Value;

                n++;
            }

            return false;
        }

        public void AppendCard(Card card)
        {
            if (CanAppendFirst(card))
            {
                _cards.AddFirst(card);
            }
            else if (CanAppendLast(card))
            {
                _cards.AddLast(card);
            }
        }

        private readonly CardCollection _cards = new CardCollection();

        public IEnumerable<Card> Cards
        {
            get { return _cards.AsEnumerable(); }
        }
    }
}
