﻿using System.Collections.Generic;
using System.Linq;
using Phase10.Core.Cards;
using Phase10.Core.Collections;
using PostSharp.Patterns.Contracts;

namespace Phase10.Core.Rules.Combinations
{
    public class Set : ICombination
    {
        public Set([NotNull] IEnumerable<Card> cards)
        {
            var cardList = cards.ToList();

            if (IsValidSet(cardList))
            {
                cardList.ForEach(_cards.Add);
            }
        }

        private static bool IsValidSet([NotNull] IList<Card> cards)
        {
            if (!cards.All(c => (c is NumberCard || c is Wildcard)))
                return false;

            var firstNumericCard = cards.Where(c => c is NumberCard)
                .Cast<NumberCard>().FirstOrDefault();
            return firstNumericCard != null &&
                   cards.All(c => (c is Wildcard) || ((NumberCard)c).Value == firstNumericCard.Value);
        }

        public bool CanAppendCard(Card card)
        {
            if (card is Wildcard)
                return true;

            if (!(card is NumberCard))
                return false;

            var firstCard = _cards.First(c => c is NumberCard) as NumberCard;
            if (firstCard == null)
                return false;

            return firstCard.Value == ((NumberCard) card).Value;
        }

        public void AppendCard(Card card)
        {
            if (CanAppendCard(card))
            {
                _cards.AddLast(card);
            }
        }

        private readonly CardCollection _cards = new CardCollection();

        public IEnumerable<Card> Cards
        {
            get { return _cards.AsEnumerable(); }
        }
    }
}
