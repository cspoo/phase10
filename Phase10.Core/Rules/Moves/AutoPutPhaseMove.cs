﻿using System.Reflection;
using NLog;
using Phase10.Core.Players;

namespace Phase10.Core.Rules.Moves
{
    public class AutoPutPhaseMove : PutPhaseMove
    {
        private static readonly Logger Logger = LogManager.GetLogger(Assembly.GetExecutingAssembly().FullName);

        public override bool Perform(Game game, Player player)
        {
            var phase = player.CurrentPhase;

            if (!phase.MatchesHand(player.Hand))
            {
                Logger.Log(LogLevel.Error, "Cannot put phase because the hand does not match");
                return false;
            }

            // Detect cards that belong to phase and add them to the phase object
            phase.FillFromHand(player.Hand);
            return base.Perform(game, player);
        }
    }
}
