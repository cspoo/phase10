﻿using NLog;
using Phase10.Core.Players;

namespace Phase10.Core.Rules.Moves
{
    public class DrawCardMove : IMove
    {
        private readonly static Logger Logger = LogManager.GetLogger("Phase10.Core");

        public bool Perform(Game game, Player player)
        {
            var card = game.DrawPile.DrawCard();
            Logger.Log(LogLevel.Info, "Drawed card {0}", card);
            player.Hand.AddLast(card);

            return true;
        }
    }
}
