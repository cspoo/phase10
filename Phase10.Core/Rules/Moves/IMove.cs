﻿using Phase10.Core.Players;
using PostSharp.Patterns.Contracts;

namespace Phase10.Core.Rules.Moves
{
    public interface IMove
    {
        bool Perform([NotNull] Game game, [NotNull] Player player);
    }
}
