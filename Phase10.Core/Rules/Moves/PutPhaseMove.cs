﻿using System.Linq;
using System.Reflection;
using NLog;
using Phase10.Core.Common.Extensions;
using Phase10.Core.Players;

namespace Phase10.Core.Rules.Moves
{
    public class PutPhaseMove : IMove
    {
        private static readonly Logger Logger = LogManager.GetLogger(Assembly.GetExecutingAssembly().FullName);

        public virtual bool Perform(Game game, Player player)
        {
            var phase = player.CurrentPhase;

            if (phase.Combinations.Count == 0)
            {
                Logger.Log(LogLevel.Error, "Phase does not contain any associated cards");
                return false;
            }

            Logger.Log(LogLevel.Info, "Putting phase {0} down", player.CurrentPhase.GetType().Name);
            player.CurrentPhase.Combinations.SelectMany(c => c.Cards).ForEach(card => player.Hand.Remove(card));

            return true;
        }
    }
}
