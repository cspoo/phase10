﻿using System.Collections.Generic;
using Phase10.Core.Collections;
using Phase10.Core.Rules.Combinations;
using PostSharp.Patterns.Collections;
using PostSharp.Patterns.Contracts;

namespace Phase10.Core.Rules.Phases
{
    public abstract class Phase
    {
        public static readonly ReadOnlyArray<Phase> Phases = new ReadOnlyArray<Phase>(new Phase[]
        {
            new Phase1(),
            new Phase7()
        });

        public List<ICombination> Combinations { get; set; }

        public abstract bool MatchesHand([NotNull] CardCollection hand);
        public abstract void FillFromHand([NotNull] CardCollection hand);

        protected Phase()
        {
            Combinations = new List<ICombination>();
        }
    }
}
