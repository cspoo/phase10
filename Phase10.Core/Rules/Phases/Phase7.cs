﻿using System;
using System.Linq;
using Phase10.Core.Cards;
using Phase10.Core.Collections;
using Phase10.Core.Common.Extensions;
using Phase10.Core.Rules.Combinations;

namespace Phase10.Core.Rules.Phases
{
    public class Phase7 : Phase
    {
        public override bool MatchesHand(CardCollection hand)
        {
            var numberStacks =
                hand.OfType<NumberCard>()
                    .GroupBy(c => c.Value, card => card, (s, cards) => cards.Count())
                    .OrderByDescending(c => c);
            if (!numberStacks.Any())
                return false;

            var wildcards = hand.OfType<Wildcard>().Count();

            var firstStack = numberStacks.FirstOrDefault();
            if (firstStack + wildcards >= 8)
                return true;
            if (firstStack + wildcards < 4)
                return false;
            var remainingWildcards = Math.Max(0, wildcards - Math.Max(0, 4 - firstStack));

            var secondStack = numberStacks.Skip(1).FirstOrDefault();
            return secondStack + remainingWildcards >= 4;
        }

        public override void FillFromHand(CardCollection hand)
        {
            var numberStacks =
                hand.OfType<NumberCard>()
                    .GroupBy(c => c.Value)
                    .ToDictionary(c => c.Key, c => c.Cast<Card>().ToList())
                    .OrderByDescending(c => c.Value.Count);
            if (!numberStacks.Any())
                return;

            var wildcards = hand.OfType<Wildcard>().ToList();
            var stacks = numberStacks.Take(2).Select(s => s.Value).ToList();

            var firstStack = stacks.FirstOrDefault();
            if (firstStack == null)
                return;
            if (firstStack.Count < 4)
            {
                wildcards.Take(4 - firstStack.Count).ForEach(w =>
                {
                    firstStack.Add(w);
                    wildcards.Remove(w);
                });
            }

            var secondStack = stacks.Skip(1).FirstOrDefault();
            if (secondStack == null)
                return;
            secondStack.AddRange(wildcards);

            Combinations.Add(new Set(firstStack));
            Combinations.Add(new Set(secondStack));
        }
    }
}
