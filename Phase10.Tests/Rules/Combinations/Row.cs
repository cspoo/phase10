﻿using NUnit.Framework;
using Phase10.Core.Cards;
using Phase10.Core.Collections;

namespace Phase10.Tests.Rules.Combinations
{
    [TestFixture]
    public class Row
    {
        [Test]
        public void TestValidCards()
        {
            var row = new Core.Rules.Combinations.Row(new CardCollection(
                new NumberCard(CardColor.Green, 3),
                new NumberCard(CardColor.Yellow, 4),
                new Wildcard()
            ));

            Assert.True(row.CanAppendCard(new NumberCard(CardColor.Purple, 2)));
            Assert.True(row.CanAppendCard(new NumberCard(CardColor.Purple, 6)));
        }

        [Test]
        public void TestInvalidCards()
        {
            var row = new Core.Rules.Combinations.Row(new CardCollection(
                new NumberCard(CardColor.Green, 3),
                new NumberCard(CardColor.Yellow, 4),
                new Wildcard()
            ));

            Assert.False(row.CanAppendCard(new NumberCard(CardColor.Purple, 5)));
            Assert.False(row.CanAppendCard(new SuspendCard()));
            Assert.False(row.CanAppendCard(new NumberCard(CardColor.Green, 1)));
        }
    }
}
