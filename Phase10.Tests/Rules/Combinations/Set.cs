﻿using NUnit.Framework;
using Phase10.Core.Cards;
using Phase10.Core.Collections;

namespace Phase10.Tests.Rules.Combinations
{
    [TestFixture]
    public class Set
    {
        [Test]
        public void TestValidCards()
        {
            var set = new Core.Rules.Combinations.Set(new CardCollection(
                new NumberCard(CardColor.Green, 4),
                new NumberCard(CardColor.Red, 4)
            ));

            Assert.True(set.CanAppendCard(new NumberCard(CardColor.Yellow, 4)));
            Assert.True(set.CanAppendCard(new Wildcard()));
        }

        [Test]
        public void TestInvalidCards()
        {
            var set = new Core.Rules.Combinations.Set(new CardCollection(
                new NumberCard(CardColor.Green, 12),
                new NumberCard(CardColor.Red, 12),
                new Wildcard()
            ));

            Assert.False(set.CanAppendCard(new NumberCard(CardColor.Yellow, 4)));
            Assert.False(set.CanAppendCard(new SuspendCard()));
        }
    }
}
