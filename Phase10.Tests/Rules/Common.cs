﻿using NUnit.Framework;
using Phase10.Core.Rules.Phases;

namespace Phase10.Tests.Rules
{
    [TestFixture]
    public class Common
    {
        [Test]
        public void CommonTests()
        {
            Assert.Greater(Phase.Phases.Length, 0);
        }
    }
}
