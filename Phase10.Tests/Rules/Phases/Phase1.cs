﻿using System;
using NUnit.Framework;
using Phase10.Core.Cards;
using Phase10.Core.Collections;

namespace Phase10.Tests.Rules.Phases
{
    [TestFixture]
    public class Phase1
    {
        [Test]
        public void TestInvalidHands()
        {
            var phase1 = new Core.Rules.Phases.Phase1();

            Assert.Throws<ArgumentNullException>(() =>
            {
                phase1.MatchesHand(null);
            });
        }

        [Test]
        public void TestNonmatchingHands()
        {
            var phase1 = new Core.Rules.Phases.Phase1();

            Assert.False(phase1.MatchesHand(new CardCollection
            {
                new NumberCard(CardColor.Green, 4),
                new NumberCard(CardColor.Purple, 4),
                new NumberCard(CardColor.Yellow, 3),
                new NumberCard(CardColor.Green, 3)
            }));

            Assert.False(phase1.MatchesHand(new CardCollection
            {
                new NumberCard(CardColor.Green, 4),
                new NumberCard(CardColor.Purple, 4),
                new NumberCard(CardColor.Red, 4),
                new NumberCard(CardColor.Yellow, 4),
                new NumberCard(CardColor.Yellow, 3),
                new NumberCard(CardColor.Green, 3)
            }));

            Assert.False(phase1.MatchesHand(new CardCollection
            {
                new NumberCard(CardColor.Green, 4),
            }));
        }

        [Test]
        public void TestMatchingHands()
        {
            var phase1 = new Core.Rules.Phases.Phase1();

            Assert.True(phase1.MatchesHand(new CardCollection
            {
                new NumberCard(CardColor.Red, 4),
                new NumberCard(CardColor.Green, 4),
                new NumberCard(CardColor.Purple, 4),
                new NumberCard(CardColor.Yellow, 3),
                new NumberCard(CardColor.Green, 3),
                new NumberCard(CardColor.Purple, 3),
                new NumberCard(CardColor.Red, 2),
                new NumberCard(CardColor.Green, 2)
            }));
        }

        [Test]
        public void TestMatchingHandsWithWildcards()
        {
            var phase1 = new Core.Rules.Phases.Phase1();

            Assert.True(phase1.MatchesHand(new CardCollection
            {
                new NumberCard(CardColor.Green, 4),
                new NumberCard(CardColor.Purple, 4),
                new NumberCard(CardColor.Yellow, 3),
                new NumberCard(CardColor.Green, 3),
                new Wildcard(),
                new Wildcard()
            }));
        }
    }
}
