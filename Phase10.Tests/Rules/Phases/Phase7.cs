﻿using System;
using NUnit.Framework;
using Phase10.Core.Cards;
using Phase10.Core.Collections;

namespace Phase10.Tests.Rules.Phases
{
    [TestFixture]
    public class Phase7
    {
        [Test]
        public void TestInvalidHands()
        {
            var phase7 = new Core.Rules.Phases.Phase7();

            Assert.Throws<ArgumentNullException>(() =>
            {
                phase7.MatchesHand(null);
            });
        }

        [Test]
        public void TestNonmatchingHands()
        {
            var phase7 = new Core.Rules.Phases.Phase7();

            Assert.False(phase7.MatchesHand(new CardCollection
            {
                new NumberCard(CardColor.Green, 4),
                new NumberCard(CardColor.Purple, 4),
                new NumberCard(CardColor.Purple, 4),
                new NumberCard(CardColor.Yellow, 3),
                new NumberCard(CardColor.Yellow, 3),
                new NumberCard(CardColor.Green, 3)
            }));

            Assert.False(phase7.MatchesHand(new CardCollection
            {
                new NumberCard(CardColor.Green, 4),
                new NumberCard(CardColor.Purple, 4),
                new NumberCard(CardColor.Red, 4),
                new NumberCard(CardColor.Red, 4),
                new NumberCard(CardColor.Yellow, 4),
                new NumberCard(CardColor.Yellow, 3),
                new NumberCard(CardColor.Yellow, 3),
                new NumberCard(CardColor.Green, 3)
            }));

            Assert.False(phase7.MatchesHand(new CardCollection
            {
                new NumberCard(CardColor.Green, 4),
            }));
        }

        [Test]
        public void TestMatchingHands()
        {
            var phase7 = new Core.Rules.Phases.Phase7();

            Assert.True(phase7.MatchesHand(new CardCollection
            {
                new NumberCard(CardColor.Red, 4),
                new NumberCard(CardColor.Green, 4),
                new NumberCard(CardColor.Green, 4),
                new NumberCard(CardColor.Purple, 4),
                new NumberCard(CardColor.Yellow, 3),
                new NumberCard(CardColor.Yellow, 3),
                new NumberCard(CardColor.Green, 3),
                new NumberCard(CardColor.Purple, 3),
                new NumberCard(CardColor.Red, 2),
                new NumberCard(CardColor.Green, 2)
            }));
        }

        [Test]
        public void TestMatchingHandsWithWildcards()
        {
            var phase7 = new Core.Rules.Phases.Phase7();

            Assert.True(phase7.MatchesHand(new CardCollection
            {
                new NumberCard(CardColor.Green, 4),
                new NumberCard(CardColor.Purple, 4),
                new NumberCard(CardColor.Yellow, 3),
                new NumberCard(CardColor.Green, 3),
                new Wildcard(),
                new Wildcard(),
                new Wildcard(),
                new Wildcard()
            }));
        }
    }
}
