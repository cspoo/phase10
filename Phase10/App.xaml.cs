﻿using System;
using System.Drawing;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Phase10
{
    public partial class App
    {
        public static T LoadResource<T>(string path)
        {
            var res = default(T);

            var sri = GetResourceStream(new Uri(path));
            if (sri == null)
                return res;

            if (sri.ContentType == @"application/xaml+xml")
            {
                res = (T)XamlReader.Load(sri.Stream);
            }
            else if (sri.ContentType.IndexOf("image", StringComparison.InvariantCulture) >= 0)
            {
                if (typeof(T).IsAssignableFrom(typeof(ImageSource)))
                {
                    var bi = new BitmapImage();
                    bi.BeginInit();
                    bi.StreamSource = sri.Stream;
                    bi.EndInit();

                    res = (T)(object)bi;
                }
                else if (typeof(T).IsAssignableFrom(typeof(Image)) || typeof(T).IsAssignableFrom(typeof(Bitmap)))
                {
                    res = (T)(object)Image.FromStream(sri.Stream);
                }
            }

            sri.Stream.Close();
            sri.Stream.Dispose();

            return res;
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            var bootstrapper = new Bootstrapper();
            bootstrapper.Run();

            ShutdownMode = ShutdownMode.OnMainWindowClose;
        }
    }
}
