﻿namespace Phase10.Infrastructure.Behaviors
{
    public interface IViewRegionRegistration
    {
        string RegionName { get; }
    }
}
