﻿using System;
using System.Drawing;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media.Imaging;
using Phase10.Core.Cards;
using PostSharp.Patterns.Contracts;

namespace Phase10.Infrastructure.Extensions
{
    public static class CardExtensions
    {
        private static Bitmap GetCardBitmap(string filename)
        {
            var assemblyName = Assembly.GetExecutingAssembly().GetName().Name;
            var imageUrl = string.Format("pack://application:,,,/{0};component/Resources/Cards/{1}", assemblyName, filename);

            return App.LoadResource<Bitmap>(imageUrl);
        }

        public static Bitmap ToCardBitmap(this NumberCard card)
        {
            return null;
        }

        public static Bitmap ToCardBitmap(this Wildcard card)
        {
            return GetCardBitmap("card_4_14.png");
        }

        public static Bitmap ToCardBitmap(this SuspendCard card)
        {
            return GetCardBitmap("card_4_13.png");
        }

        public static BitmapImage ToCardBitmapImage(this Card card)
        {
            if (card is NumberCard)
                return ((NumberCard)card).ToCardBitmap().ToBitmapImage();
            if (card is SuspendCard)
                return ((SuspendCard)card).ToCardBitmap().ToBitmapImage();
            if (card is Wildcard)
                return ((Wildcard)card).ToCardBitmap().ToBitmapImage();

            return null;
        }
    }
}
