﻿using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Windows.Input;
using System.Windows.Media;
using Microsoft.Practices.Prism.Commands;
using Phase10.Core;
using Phase10.Core.Cards;
using Phase10.Core.Players;
using Phase10.Infrastructure.Extensions;
using PostSharp.Patterns.Model;

namespace Phase10.UI.ViewModel
{
    [Export]
    [NotifyPropertyChanged]
    internal class Shell
    {
        public ICommand FileExitCommand { get; private set; }

        public Game Game { get; private set; }

        [SafeForDependencyAnalysis]
        public ImageSource CardImage
        {
            get
            {
                return (new Wildcard()).ToCardBitmapImage();
            }
        }

        public ObservableCollection<Card> Cards
        {
            get { return new ObservableCollection<Card>(Game.CurrentPlayer.Hand);}
        }
        
        [ImportingConstructor]
        public Shell(Game game)
        {
            Game = game;
            FileExitCommand = new DelegateCommand(FileExit);

            Game.AddPlayer(new Player
            {
                Name = "Alice"
            });
        }

        private void FileExit()
        {
            View.Close();
        }

        [Import(AllowRecomposition = true)]
        public Windows.Shell View { get; set; }
    }
}
