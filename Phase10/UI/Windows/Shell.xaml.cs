﻿using System.ComponentModel.Composition;

namespace Phase10.UI.Windows
{
    [Export]
    public partial class Shell
    {
        public Shell()
        {
            InitializeComponent();
        }

        [Import]
        private ViewModel.Shell ViewModel
        {
            set { DataContext = value; }
        }
    }
}
